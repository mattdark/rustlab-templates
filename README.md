# RustLab Templates

A repository with HTML5 Templates for exercises of the "Create and Deploy your first Rust Web App" workshop at RustLab 2020

---

### Form
Built with [Bootstrap](https://getbootstrap.com)

### Business Card
Based on: [2 Sided Digital Business Card](https://codepen.io/designcouch/pen/sqxdi)

### Error Page
Based on: [Error Pages](https://codepen.io/johnfinkdesign/pen/XjkBPE)